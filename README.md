![Layout icon](https://gitlab.com/uploads/project/avatar/1075126/layout__1.png "Sass layout")

# Sass layout
### Sass modular layout with BEM naming convention.


# How to use
	After install this module in your project find main css or scss file and import module.
	Path to module is: node_modules/Sass-layout/dist/layout.

### Example:
		@import 'node_modules/Sass-layout/dist/layout';
	
#### More information you can find at our [Demo](https://www.layout.kids-playground.net)