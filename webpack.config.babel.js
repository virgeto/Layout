const path = require('path');
const webpack = require('webpack');
const hot = new webpack.HotModuleReplacementPlugin();
const plugins = [];

plugins.push(hot);
if (process.argv.indexOf('-p') !== -1) {
	console.log('Production mode, ON!');
}

const config = {
	entry: ['webpack/hot/dev-server', path.resolve('./client/entry.js')],
	output: {
		path: path.resolve('./client/build'),
		filename: 'bundle.js',
		publicPath: 'build/',
	},
	module: {
		loaders: [
			{
				test: /\.scss$/,
				loader: 'style!css!autoprefixer?{browsers:["last 2 versions", "Safari >= 8", "Firefox >= 38", "Explorer >= 11"]}!sass',
				exclude: /node_modules/
			}, {
				test: /\.css$/,
				loader: 'style!css!autoprefixer?{browsers:["last 2 versions", "Safari >= 8", "Firefox >= 38", "Explorer >= 11"]}'
			}, {
				test: /\.html$/,
				loader: 'html'
			}, {
				test: /\.js$/,
				loader: 'ng-annotate?add=true!babel?presets[]=es2015',
				exclude: /node_modules/
			}, {
				test: /\.(jpe?g|png|gif)$/i,
				loader: 'url?limit=10000!img?progressive=true'
			}, {
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: 'url?limit=10000&minetype=application/font-woff'
			}, {
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: 'file-loader?name=[path][name].[ext]'
			}
		],
		preLoaders: [
			{
				test: /\.js$/,
				loader: 'eslint-loader',
				exclude: /node_modules/
			}
		]
	},
	plugins: plugins,
	devServer: {
		hot: true,
		contentBase: './client',
		stats: {
			chunkModules: false,
			colors: true,
			progress: true
		},
		historyApiFallback: true
	}
};

module.exports = config;