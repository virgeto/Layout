angular.module('Layout', [
	'ui.router',
	'Components',
	'pascalprecht.translate'
])
  .config(mainConfig)
	.run(runConfig);

// Configuration
function mainConfig($httpProvider, $urlRouterProvider, $locationProvider, $stateProvider, $translateProvider) {
	'ngInject';

	$urlRouterProvider.otherwise('/home');
	$locationProvider.html5Mode(true);
	$stateProvider
		.state('app', {
			abstract: true,
			template: '<div ui-view></div>'
		});
	$translateProvider.preferredLanguage('en');
}

/** */
function runConfig($rootScope, $state) {
	'ngInject';
	
	$rootScope.$state = $state;
}