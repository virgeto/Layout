function ComponentsComponent() {
	'ngInject';
	const service = {
		'constructor': [
			{
				'label': 'Buttons',
				'link': 'app.component.buttons',
				'icon': 'fa-bullseye'
			},
			{
				'label': 'Typography',
				'link': 'app.component.typography',
				'icon': 'fa-text-width'
			},
			{
				'label': 'Structures',
				'link': 'app.component.structures',
				'icon': 'fa-cubes'
			},
			{
				'label': 'Grid',
				'link': 'app.component.grid',
				'icon': 'fa-table'
			},
			{
				'label': 'Lists',
				'link': 'app.component.lists',
				'icon': 'fa-list'
			},
			{
				'label': 'Palette',
				'link': 'app.component.palette',
				'icon': 'fa-paint-brush'
			},
			{
				'label': 'Fixes',
				'link': 'app.component.fixes',
				'icon': 'fa-wrench'
			}
		]
	};
	
	return service;
}

export default ComponentsComponent;