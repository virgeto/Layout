import routeConfig from './layout.route';

angular.module('Layout')
	.config(routeConfig);
	
require('./layout-components');