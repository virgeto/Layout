import template from './layout.html';
import controller from './layout.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component', {
			abstract: true,
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;