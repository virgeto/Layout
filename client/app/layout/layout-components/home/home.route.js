import template from './home.html';
import controller from './home.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.home', {
			url: '/home',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;