import template from './downloads.html';
import controller from './downloads.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.downloads', {
			url: '/downloads',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;