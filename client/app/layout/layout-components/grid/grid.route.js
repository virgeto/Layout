import template from './grid.html';
import controller from './grid.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.grid', {
			url: '/grid',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;