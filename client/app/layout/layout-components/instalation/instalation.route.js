import template from './instalation.html';
import controller from './instalation.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.instalation', {
			url: '/instalation',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;