import template from './lists.html';
import controller from './lists.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.lists', {
			url: '/lists',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;