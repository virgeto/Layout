import template from './palette.html';
import controller from './palette.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.palette', {
			url: '/palette',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;