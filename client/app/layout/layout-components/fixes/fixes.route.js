import template from './fixes.html';
import controller from './fixes.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.fixes', {
			url: '/fixes',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;