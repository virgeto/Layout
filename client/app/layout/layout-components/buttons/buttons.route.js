import template from './buttons.html';
import controller from './buttons.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.buttons', {
			url: '/buttons',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;