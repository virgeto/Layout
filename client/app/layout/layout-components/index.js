require('./how-to');
require('./buttons');
require('./typography');
require('./lists');
require('./structures');
require('./grid');
require('./fixes');
require('./palette');
require('./downloads');
require('./home');
require('./instalation');