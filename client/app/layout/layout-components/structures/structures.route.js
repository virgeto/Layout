import template from './structures.html';
import controller from './structures.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.structures', {
			url: '/structures',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;