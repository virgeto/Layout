import routeConfig from './structures.route.js';

angular.module('Layout')
	.config(routeConfig);
