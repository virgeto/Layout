import template from './typography.html';
import controller from './typography.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.typography', {
			url: '/typography',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;