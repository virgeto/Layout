import routeConfig from './typography.route.js';

angular.module('Layout')
	.config(routeConfig);
