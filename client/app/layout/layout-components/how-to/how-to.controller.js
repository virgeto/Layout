function HowToController() {
	'ngInject';
	const vm = this;
	vm.contentItems = [
		{
			'label': 'Page structure',
			'link': ''
		},
		{
			'label': 'Naming convention',
			'link': ''
		},
		{
			'label': 'Style giude',
			'link': ''
		},
		{
			'label': 'Styling elements',
			'link': ''
		},
		{
			'label': 'Code strcture',
			'link': ''
		}
	];
}

export default HowToController;