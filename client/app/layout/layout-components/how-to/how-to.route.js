import template from './how-to.html';
import controller from './how-to.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.component.components', {
			url: '/components',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;