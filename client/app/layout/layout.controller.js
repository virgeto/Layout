function LayoutController($state, $location, $scope, ComponentsComponent) {
	'ngInject';
	
	const vm = this;
	vm.hiddenComponents = false;
	vm.showMobileMenu = false;
	vm.showMobileComponentsMenu = false;
	vm.components = ComponentsComponent.constructor;
	vm.stateChange = stateChange;
	
	$scope.$watch(function() {
		const currentPath = $location.path().replace('/', '');
		if (vm.state  !== currentPath ) {
			vm.state = currentPath;
			watchActiveTopMenuLink(vm.state);
		}
	});
	
	vm.links = [
		'Buttons',
		'Typography',
		'Containers',
		'Structures',
		'Grid',
		'Lists',
		'Palette',
		'Fixes'
	];
	
	vm.topMenuItems = [
		'Home',
		'Components',
		'Instalation',
		'Downloads'
	];
	
	/** Watch  for top menu links active */
	function watchActiveTopMenuLink(state) {
	
		for ( const i in vm.topMenuItems ) {
			if ( state === vm.topMenuItems[i].toLowerCase() && state !== 'components') {
				vm.hiddenComponents = true;
				break;
			} else {
				vm.hiddenComponents = false;				
			}
			console.log(vm.hiddenComponents);
		}
	}
	
	/** */
	function stateChange(state) {
		console.log(state);
		if ( state.indexOf('components') === -1 ) {
			$state.go(`app.component.${ state }`);
			vm.showMobileMenu = !vm.showMobileMenu;
		} else {
			vm.showMobileComponentsMenu = true;
		}
	}
}

export default LayoutController;
